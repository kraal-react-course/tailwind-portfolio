import { nanoid } from 'nanoid';
import { FaHtml5, FaJs, FaReact } from 'react-icons/fa';
import proj1 from "./assets/projects/store.jpg";
import proj2 from "./assets/projects/gallery.jpg";
import proj3 from "./assets/projects/colors.jpg";
import proj4 from "./assets/projects/menu.jpg";
import proj5 from "./assets/projects/strapi.jpg";
import proj6 from "./assets/projects/cart.jpg";
import proj7 from "./assets/projects/cms.jpg";
import proj8 from "./assets/projects/jobapp.jpg";

export const links = [
  { id: nanoid(), href: '#home', text: 'home' },
  { id: nanoid(), href: '#skills', text: 'skills' },
  { id: nanoid(), href: '#about', text: 'about' },
  { id: nanoid(), href: '#projects', text: 'projects' },
];

export const skills = [
  {
    id: nanoid(),
    title: 'HTML&CSS',
    icon: <FaHtml5 className='h-16 w-16 text-violet-500' />,
    text: 'Highly skilled in HTML & CSS, adeptly crafting visually appealing and responsive websites for optimal user experiences.',
  },
  {
    id: nanoid(),
    title: 'Javascript',
    icon: <FaJs className='h-16 w-16 text-violet-500' />,
    text: 'Expertise in JavaScript, building interactive and dynamic web applications with a focus on seamless user interactions and functionality',
  },
  {
    id: nanoid(),
    title: 'React',
    icon: <FaReact className='h-16 w-16 text-violet-500' />,
    text: 'Enthusiastic React developer, developing efficient and interactive front-end applications with a strong emphasis on component-based architecture.',
  },
];

export const projects = [
  {
    id: nanoid(),
    img: proj1,
    url: 'https://react-course-13-furnistore.netlify.app',
    gitlab: 'https://gitlab.com/kraal-react-course',
    title: 'FurniStore',
    text: 'online store with some functions',
  },
  {
    id: nanoid(),
    img: proj8,
    url: 'https://react-course-jobapp.netlify.app/',
    gitlab: 'https://gitlab.com/kraal-react-course',
    title: 'Job App',
    text: 'Job tracking application',
  },
  {
    id: nanoid(),
    img: proj2,
    url: 'https://react-course-7-vite-image-gallery.netlify.app',
    gitlab: 'https://gitlab.com/kraal-react-course',
    title: 'vite gallery',
    text: 'gallery via API',
  },
  {
    id: nanoid(),
    img: proj3,
    url: 'https://react-course-10-colorgenerator.netlify.app',
    gitlab: 'https://gitlab.com/kraal-react-course',
    title: 'color generator',
    text: '21 shades of color',
  },
  {
    id: nanoid(),
    img: proj7,
    url: 'https://react-course-all-projects.netlify.app',
    gitlab: 'https://gitlab.com/kraal-react-course',
    title: 'contentful CMS',
    text: 'smaller practice projects',
  },
  {
    id: nanoid(),
    img: proj5,
    url: 'https://react-course-12-strapi.netlify.app',
    gitlab: 'https://gitlab.com/kraal-react-course',
    title: 'strapi',
    text: 'strapi CMS basics',
  },
  {
    id: nanoid(),
    img: proj6,
    url: 'https://react-course-5-cart.netlify.app',
    gitlab: 'https://gitlab.com/kraal-react-course',
    title: 'e-cart',
    text: 'shopping cart options',
  },
  {
    id: nanoid(),
    img: proj4,
    url: 'https://react-course-14-menu.netlify.app',
    gitlab: 'https://gitlab.com/kraal-react-course',
    title: 'menu cards',
    text: 'foods with filters',
  },
];
