import aboutSvg from "../assets/aboutSvg.svg";
import SectionTitle from "./SectionTitle";

const About = () => {
  return (
    <section className="bg-white py-20" id="about">
        <div className="align-element grid md:grid-cols-2 items-center gap-16">
            <img src={aboutSvg} className="w-full h-64" />
            <article>
                <SectionTitle  text="code and coffee" />
                <p className="text-slate-600 mt-8 leading-loose">
                Some funny or interesting text about me will be placed here. For now, it's just that I like coding and I like coffee. But only good coffee. And cats - I love them even more. You can find additional information on my website <a href="https://kraaldesign.hu" className="website" title="KRAALDESIGN" target="_blank">kraaldesign.hu<svg viewBox="0 0 35 36">
            <path d="M6.9739 30.8153H63.0244C65.5269 30.8152 75.5358 -3.68471 35.4998 2.81531C-16.1598 11.2025 0.894099 33.9766 26.9922 34.3153C104.062 35.3153 54.5169 -6.68469 23.489 9.31527" />
        </svg></a>
                </p>
            </article>
        </div>
    </section>
  )
};
export default About;