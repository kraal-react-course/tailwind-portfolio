import heroImg from '../assets/hero.svg';
import { FaGitlab, FaLinkedin, FaGlobe } from 'react-icons/fa';
const Hero = () => {
  return (
    <section className='bg-violet-100 py-24  '>
      <div className='align-element grid md:grid-cols-2 items-center gap-8'>
        <article>
          <h1 className='text-7xl font-bold tracking-wider'>I'm Alexa</h1>
          <p className='mt-4 text-3xl text-slate-700 capitalize tracking-wide'>
            Front-end developer
          </p>
          <p className='mt-2 text-lg text-slate-700 capitalize tracking-wide'>
            turning ideas into interactive reality
          </p>
          <div className='flex gap-x-4 mt-4'>
            <a href='https://gitlab.com/kraal-react-course' target='_blank'>
              <FaGitlab className='h-8 w-8 text-slate-500 hover:text-black duration-300' />
            </a>
            <a href='https://linkedin.com/' target='_blank'>
              <FaLinkedin className='h-8 w-8 text-slate-500 hover:text-black duration-300' />
            </a>
            <a href='https://kraaldesign.hu' target='_blank'>
              <FaGlobe className='h-8 w-8 text-slate-500 hover:text-black duration-300' />
            </a>
          </div>
        </article>

        <article className='hidden md:block '>
          <img src={heroImg} className='h-80 lg:h-96' />
        </article>
      </div>
    </section>
  );
};
export default Hero;